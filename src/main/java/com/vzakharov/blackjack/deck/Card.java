package com.vzakharov.blackjack.deck;

public class Card implements Comparable<Card> {

    private Rank rank;
    private Suit suit;

    Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public int getValue() {
        return rank.getValue();
    }

    public boolean isAce() {
        return rank == Rank.ACE;
    }

    public boolean isTen() {
        return rank == Rank.TEN;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    @Override
    public int compareTo(Card o) {
        if (this.rank == o.rank) return 0;
        if (this.rank.getValue() > o.rank.getValue()) return 1;
        else return -1;
    }

    @Override
    public String toString() {
        return rank.getSymbol() + suit.getSymbol();
    }
}
