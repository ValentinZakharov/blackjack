package com.vzakharov.blackjack.deck;

import java.util.LinkedList;

public class ShuffledCardsQueue extends LinkedList<Card> {

    ShuffledCardsQueue() {}

    public Card pickCard() {
        return pop();
    }

}
