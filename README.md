## Summary

- [**Overview**](#overview)
- [**Prerequisites**](#prerequisites)
- [**Building**](#building)
- [**Running**](#running)
- [**Running**](#running)
- [**Credits**](#credits)

## Overview
A recreation of the classic card game 21, otherwise known as Blackjack, in Java.
Rules source used during development: https://www.pagat.com/banking/blackjack.html

![Screenshot](http://bitbucket.org/ValentinZakharov/blackjack/raw/7a8df7627458deb9787c330b28f70004c7ff95e5/screenshot.png =550x)

## Prerequisites
- Operation System: Windows/Linux/MacOs
- Virtual Machine: Java 1.8
- Build tool: Maven 3.6.1 (or higher)

## Building
- Clone this repo and go to blackjack directory
```
$ git clone https://ValentinZakharov@bitbucket.org/ValentinZakharov/blackjack.git
$ cd core-devops/blackjack
```
- Build project using Maven
```
$ mvn clean compile package
```

## Running
- You can start the app from maven
```
$ mvn exec:java
```
- Or you can directly execute the jar-file
```
$ java -jar target/BlackJackAlice-1.0-SNAPSHOT-jar-with-dependencies.jar
```

## Credits

* **Valentin Zakharov** ([email@vzakharov.net](mailto:email@vzakharov.net))