package com.vzakharov.blackjack.bank;

import java.util.List;

public class Bank {

    private long money;
    private long betSize;

    public Bank(long money, long defaultBetSize) {
        this.money = money;
        this.betSize = defaultBetSize;
    }

    public long getMoney() {
        return money;
    }

    public Bet getBet() {
        if (money - betSize < 0) {
            return null;
        }
        money -= betSize;
        return new Bet(betSize);
    }

    public Bet getBet(long betSize) {
        if (money - betSize <= 0) {
            return null;
        }
        money -= betSize;
        return new Bet(betSize);
    }

    public void putBet(Bet bet) {
        money += bet.getBetSize();

    }

    public void putReward(Reward reward) {
        money += reward.getRewardSize();
    }

    public void putRewards(List<Reward> rewards) {
        for (Reward reward : rewards) {
            putReward(reward);
        }
    }

    @Override
    public String toString() {
        return "$" + money;
    }
}
