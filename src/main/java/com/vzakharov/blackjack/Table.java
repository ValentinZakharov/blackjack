package com.vzakharov.blackjack;

import com.vzakharov.blackjack.bank.Bet;
import com.vzakharov.blackjack.bank.Reward;
import com.vzakharov.blackjack.deck.*;

import java.util.LinkedList;
import java.util.List;

public class Table {

    private Deck deck = new Deck();
    private ShuffledCardsQueue queue;

    private Hand playerHand1 = new PlayerHand();
    private Hand playerHand2;
    private Hand dealerHand = new DealerHand();

    private Bet bet;

    /**
     * Shuffle deck and prepare cards queue
     * for the game
     */
    public void shuffle() {
        queue = deck.getShuffledCardsQueue();
    }

    /**
     * Get number of remaining cards in queue
     */
    public int remainingCards() {
        return queue.size();
    }

    /**
     * Deal one card to player's hand
     */
    public Card dealCardToPlayer() {
        return playerHand1.dealCard(queue);
    }

    /**
     * Deal one card to dealer's hand
     */
    public Card dealCardToDealer() {
        return dealerHand.dealCard(queue);
    }

    /**
     * Clear all hands (player's and dealer's)
     * Also delete splitted hand if exist
     */
    public void clearHands() {
        playerHand1.clearHand();
        if (playerHand2 != null) {
            playerHand2.clearHand();
            playerHand2 = null;
        }
        dealerHand.clearHand();
    }

    /**
     * Set bet for current round
     */
    public void setBet(Bet bet) {
        this.bet = bet;
    }

    /**
     * Get current bet
     */
    public Bet getBet() {
        return this.bet;
    }

    /**
     * Calculate and get reward for given game state
     */
    public Reward getReward(GameState state) {
        if (state == GameState.PLAYER_WON) {
            return new Reward(bet.getBetSize());
        }
        if (state == GameState.BLACKJACK_PLAYER_WON) {
            return new Reward(bet.getBetSize() + bet.getBetSize()/2);
        }
        return new Reward(0);
    }

    /**
     * Calculate and get all rewards for all player's hands
     */
    public List<Reward> getRewards() {
        List<Reward> rewards = new LinkedList<>();
        for (GameState state : getStates()) {
            if (state == GameState.PLAYER_WON) {
                rewards.add(new Reward(bet.getBetSize()));
            }
            if (state == GameState.BLACKJACK_PLAYER_WON) {
                rewards.add(new Reward(bet.getBetSize() + bet.getBetSize()/2));
            }
        }
        return rewards;
    }

    /**
     * Check is player can Split his hand
     * Should be only one splittable player's hand
     */
    public boolean canPlayerSplit() {
        return playerHand2 == null && playerHand1.canSplit();
    }

    /**
     * Split player's hand into two hands
     */
    public void split() {
        playerHand2 = playerHand1.split();
    }

    /**
     * Check if player can Swap splitted hands
     */
    public boolean canPlayerSwap() {
        if (playerHand1 == null || playerHand2 == null) {
            return false;
        } else {
            return playerHand1.getNumberOfCards() == 1 ||
                    playerHand2.getNumberOfCards() == 1;
        }
    }

    /**
     * Swap player's splitted hands
     */
    public void swapPlayerSplit() {
        Hand temp = playerHand1;
        playerHand1 = playerHand2;
        playerHand2 = temp;
    }

    /**
     * Check if player can Stand
     * Means that dealer start it's turn
     */
    public boolean canPlayerStand() {
        return playerHand1.getNumberOfCards() >= 2 &&
                (playerHand2 == null || playerHand2.getNumberOfCards() >= 2);
    }

    /**
     * Check if player can Double his bet.
     * When it's happens - player get instantly one more
     * card and starts dealer's turn
     */
    public boolean canPlayerDouble() {
        return playerHand1.getNumberOfCards() == 2 && playerHand2 == null;
    }

    /**
     * Get game state - the state Win/Lose between player's
     * first hand and dealer's hand.
     */
    private GameState getGameState() {
        return checkWinner(playerHand1, dealerHand);
    }

    /**
     * Get game state - the state Win/Lost between player's
     * second(splitted) hand and dealer's hand.
     */
    private GameState getSplitState() {
        if (playerHand2!= null) {
            return checkWinner(playerHand2, dealerHand);
        }
        return GameState.IN_PROGRESS;
    }

    /**
     * Get all states of the game - means states for
     * the first player's hand and the second(splitted)
     * player's hand
     */
    public GameState[] getStates() {
        GameState[] states = new GameState[2];
        states[0] = getGameState();
        states[1] = getSplitState();
        return states;
    }

    /**
     * Check if one of the hand is winning
     */
    private GameState checkWinner(Hand playerHand, Hand dealerHand) {
        int playerValue = playerHand.getHandValue();
        int dealerValue = dealerHand.getHandValue();

        if (playerValue > 21) {
            return GameState.DEALER_WON;
        }
        if (dealerValue > 21) {
            return GameState.PLAYER_WON;
        }

        if (playerHand.getNumberOfCards() >= 2 &&
                dealerHand.getNumberOfCards() >= 2) {

            if (playerValue == dealerValue) {
                return GameState.PUSH;
            }
            if (playerValue == 21) {
                return GameState.BLACKJACK_PLAYER_WON;
            }
            if (dealerValue == 21) {
                return GameState.BLACKJACK_DEALER_WON;
            }
            if (dealerValue > playerValue) {
                return GameState.DEALER_WON;
            }
        }
        return GameState.IN_PROGRESS;
    }

    @Override
    public String toString() {
        return "Dealer: \t" + dealerHand +
                "\nPlayer: \t" + playerHand1 +
                (playerHand2 != null ? "\n        \t" + playerHand2 : "") +
                "\nBet: " + bet;
    }

    public String toDrawTable() {
        return dealerHand.toDrawHand("Dealer") +
               playerHand1.toDrawHand("Player") +
                (playerHand2 != null ? playerHand2.toDrawHand(null) : "") +
               "\nBET: " + bet;
    }
}
