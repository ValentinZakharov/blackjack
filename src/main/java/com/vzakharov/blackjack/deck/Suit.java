package com.vzakharov.blackjack.deck;

public enum Suit {
    DIAMONDS('♦'),
    CLUBS('♣'),
    HEARTS('♥'),
    SPADES('♠');

    private char symbol;

    Suit(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}
