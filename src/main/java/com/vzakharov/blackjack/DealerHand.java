package com.vzakharov.blackjack;

import com.vzakharov.blackjack.deck.Card;

public class DealerHand extends Hand {

    @Override
    boolean canSplit() {
        return false;
    }

    @Override
    Hand split() {
        return null;
    }

    @Override
    public String toString() {
        String str = super.toString();

        // Add fake closed card
        if (cards.size() == 1) {
            str = str + "??";
        }
        return str;
    }

    public String toDrawHand(String handName) {
        StringBuilder sb = new StringBuilder("        ");
        int numOfCards = cards.size();
        // Add fake closed card
        if (numOfCards == 1) numOfCards = 2;
        for (int i=0; i<numOfCards; i++) {
            sb.append("╭───╮");
        }
        sb.append("\n").append(handName).append(": ");
        for (Card card : cards) {
            if (card.isTen()) {
                sb.append("│10 │");
            } else {
                sb.append("│")
                  .append(card.getRank().getSymbol()).append("  │");
            }
        }
        // Draw fake closed card
        if (cards.size() == 1) {
            sb.append("│   │");
        }
        sb.append("\n        ");
        for (Card card : cards) {
            sb.append("│  ").append(card.getSuit().getSymbol()).append("│");
        }
        // Draw fake closed card
        if (cards.size() == 1) {
            sb.append("│   │");
        }
        sb.append("  =>  (").append(getHandValue()).append(")\n        ");
        for (int i=0; i<numOfCards; i++) {
            sb.append("╰───╯");
        }
        sb.append("\n");
        return sb.toString();
    }
}
