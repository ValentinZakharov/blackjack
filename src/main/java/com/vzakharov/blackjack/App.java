package com.vzakharov.blackjack;

import com.vzakharov.blackjack.bank.Bank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App implements GameEventListener {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private Game game;
    private Bank bank;


    public App() {
        bank = new Bank(100, 10);
        game = new Game(this, bank);

        System.out.println("Welcome to Blackjack.");
        System.out.println("You have " + bank + " in your bank.");
        System.out.println("─────────────────────────────────────────────────");
        System.out.println("(1) Start (2) Exit");

        while (true) {
            switch (readConsoleInput()) {
                case "1":
                    game.start();
                    break;
                case "2":
                    exit();
            }
        }
    }

    /**
     * Handler for user action.
     * When game needs some decision from user (Hit, Stand, Split,...)
     * it calls this event handler and ask user do choose some action
     */
    @Override
    public void onAskCommand(Table table) {

        System.out.println("─────────────────────────────────────────────────");
        System.out.println(table.toDrawTable());
        System.out.println("Player bank: " + bank);
        System.out.println("─────────────────────────────────────────────────");
        StringBuffer sb = new StringBuffer("(1) Hit ");
        if (table.canPlayerStand()) {
            sb.append("(2) Stand ");
        }
        if (table.canPlayerDouble()) {
            sb.append("(3) Double Down ");
        }
        if (table.canPlayerSplit()) {
            sb.append("(4) Split ");
        }
        if (table.canPlayerSwap()) {
            sb.append("(5) Swap ");
        }
        sb.append("(6) Surrender");
        System.out.println(sb);

        while (true) {
            switch (readConsoleInput()) {
                case "1":
                    game.hit();
                    return;
                case "2":
                    game.stand();
                    return;
                case "3":
                    game.doubleDown();
                    return;
                case "4":
                    game.split();
                    return;
                case "5":
                    game.swapSplit();
                    return;
                case "6":
                    exit();
            }
        }
    }

    /**
     * Handler for end of round.
     * When round is over - this event calls by game to ask
     * user what should we do next (play one more round or
     * exit the game)?
     */
    @Override
    public void onRoundOver(Table table) {
        System.out.println("─────────────────────────────────────────────────");
        System.out.println(table.toDrawTable());
        System.out.println("Player bank: " + bank);

        for (GameState state : table.getStates()) {
            switch (state) {
                case PLAYER_WON:
                    System.out.println("< PLAYER WIN! " + table.getReward(state) + " >");
                    break;
                case DEALER_WON:
                case BLACKJACK_DEALER_WON:
                    System.out.println("< DEALER WINS! " + table.getBet() + " >");
                    break;
                case BLACKJACK_PLAYER_WON:
                    System.out.println("< BLACKJACK! WIN! " + table.getReward(state) + " >");
                    break;
                case PUSH:
                    System.out.println("< PUSH! Bet returned back! >");
                    break;
            }
        }

        if (bank.getMoney() >= table.getBet().getBetSize()) {
            System.out.println("─────────────────────────────────────────────────");
            System.out.println("(1) Next round (2) Exit");
            while (true) {
                switch (readConsoleInput()) {
                    case "1":
                        game.start();
                    case "2":
                        exit();
                }
            }
        } else {
            System.out.println("─────────────────────────────────────────────────");
            System.out.println("Player has no enough money in bank.");
            exit();
        }
    }

    /**
     * Helper method to read user's input from console
     */
    public String readConsoleInput() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Helper method to exit the game
     */
    public void exit() {
        System.out.println("Good luck next time! Bye!");
        System.exit(0);
    }

    public static void main(String[] args) {
        new App();
    }

}
