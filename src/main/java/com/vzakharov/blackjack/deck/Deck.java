package com.vzakharov.blackjack.deck;

import java.security.SecureRandom;
import java.util.*;
public class Deck {

    private List<Card> deck = new LinkedList<>();

    public Deck() {
        // Prepare 4 deck
        for (int i=0; i<4; i++) {
            EnumSet.allOf(Suit.class).forEach(s ->
                    EnumSet.allOf(Rank.class).forEach(r ->
                            deck.add(new Card(r, s))));
        }
    }

    public ShuffledCardsQueue getShuffledCardsQueue() {
        ShuffledCardsQueue queue = new ShuffledCardsQueue();
        Collections.shuffle(deck, new SecureRandom());
        queue.addAll(deck);
        return queue;
    }
}
