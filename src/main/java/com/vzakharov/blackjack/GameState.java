package com.vzakharov.blackjack;

public enum GameState {
    IN_PROGRESS,
    PLAYER_WON,
    DEALER_WON,
    BLACKJACK_PLAYER_WON,
    BLACKJACK_DEALER_WON,
    PUSH
}
