package com.vzakharov.blackjack;

import com.vzakharov.blackjack.deck.Card;
import com.vzakharov.blackjack.deck.ShuffledCardsQueue;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class Hand {

    protected LinkedList<Card> cards = new LinkedList<>();

    /**
     * Deal only one card to hand
     *
     * @param queue
     */
    public Card dealCard(ShuffledCardsQueue queue) {
        Card card = queue.pickCard();
        cards.add(card);
        return card;
    }

    public int getNumberOfCards() {
        return cards.size();
    }

    /**
     * Clear hand from cards
     */
    public void clearHand() {
        cards.clear();
    }

    /**
     * Calculate value of all cards in hand
     *
     * @return
     */
    public int getHandValue() {
        List<Card> sortedCards = new LinkedList<>(cards);
        Collections.sort(sortedCards);
        int handValue = 0;
        for (Card card : sortedCards) {
            if (card.isAce() && handValue + card.getValue() > 21) {
                // The Ace is always valued at 11 unless that would result in
                // the hand going over 21, in which case it is valued as 1.
                handValue += 1;
            } else {
                handValue += card.getValue();
            }
        }
        return handValue;
    }

    abstract boolean canSplit();

    abstract Hand split();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getHandValue()).append("] \t");
        for (Card card : cards) {
            sb.append(card.toString()).append(" ");
        }
        return sb.toString();
    }

    abstract String toDrawHand(String handName);
}
