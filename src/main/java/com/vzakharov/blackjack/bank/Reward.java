package com.vzakharov.blackjack.bank;

public class Reward {

    private long rewardSize;

    public Reward(long rewardSize) {
        this.rewardSize = rewardSize;
    }

    public long getRewardSize() {
        return rewardSize;
    }

    @Override
    public String toString() {
        return "$" + rewardSize;
    }
}
