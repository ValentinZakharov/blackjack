package com.vzakharov.blackjack;

import com.vzakharov.blackjack.deck.Card;

public class PlayerHand extends Hand {

    public boolean canSplit() {
        if (cards.size() != 2) {
            return false;
        }
        Card card1 = cards.get(0);
        Card card2 = cards.get(1);
        return card1.compareTo(card2) == 0;
    }

    public Hand split() {
        if (!canSplit())
            return null;

        Hand secondHand = new PlayerHand();
        Card card = this.cards.pop();
        secondHand.cards.push(card);

        return secondHand;
    }

    public String toDrawHand(String handName) {
        StringBuilder sb = new StringBuilder("        ");
        for (int i=0; i<cards.size(); i++) {
            sb.append("╭───╮");
        }
        sb.append("\n");
        if (handName != null && !handName.isEmpty()) {
            sb.append(handName).append(": ");
        } else {
            sb.append("        ");
        }
        for (Card card : cards) {
            if (card.isTen()) {
                sb.append("│10 │");
            } else {
                sb.append("│").append(card.getRank().getSymbol()).append("  │");
            }
        }
        sb.append("\n        ");
        for (Card card : cards) {
            sb.append("│  ").append(card.getSuit().getSymbol()).append("│");
        }
        sb.append("  =>  (").append(getHandValue()).append(")\n        ");
        for (int i=0; i<cards.size(); i++) {
            sb.append("╰───╯");
        }
        sb.append("\n");
        return sb.toString();
    }
}
