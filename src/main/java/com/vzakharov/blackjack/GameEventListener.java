package com.vzakharov.blackjack;

public interface GameEventListener {
    void onAskCommand(Table table);
    void onRoundOver(Table table);
}
