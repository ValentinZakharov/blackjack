package com.vzakharov.blackjack.bank;

public class Bet {

    private long betSize;

    public Bet(long betSize) {
        this.betSize = betSize;
    }

    public long getBetSize() {
        return betSize;
    }

    @Override
    public String toString() {
        return "$" + betSize;
    }
}
