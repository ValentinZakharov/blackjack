package com.vzakharov.blackjack;


import com.vzakharov.blackjack.bank.Bank;
import com.vzakharov.blackjack.bank.Bet;
import com.vzakharov.blackjack.bank.Reward;

import java.util.List;

public class Game {

    private Table table;
    private GameEventListener listener;
    private Bank bank;

    public Game(GameEventListener listener, Bank bank) {
        this.listener = listener;
        this.table = new Table();
        this.bank = bank;
        table.shuffle();
    }

    /**
     * Execute one round of the game from deal to win
     * one of players. Method will call high-level
     * events of {GameEventListener} which should be
     * implemented and handled by application
     */
    public void start() {
        table.clearHands();

        table.setBet(bank.getBet());

        if (table.remainingCards() < 20) {
            // Not enough cards - shuffle
            table.shuffle();
        }

        // The player and the dealer receive two cards each
        // one of dealer's card is closed, so we don't deal it at start
        table.dealCardToPlayer();
        table.dealCardToDealer();
        table.dealCardToPlayer();

        GameState[] states;

        // Player's turn
        do {
            listener.onAskCommand(table);
            states = table.getStates();
        } while (states[0] == GameState.IN_PROGRESS);


        // Game reward calculation
        if (states[0] == GameState.PLAYER_WON) {
            Bet bet = table.getBet();
            bank.putBet(bet);
        }
        if (states[0] == GameState.BLACKJACK_PLAYER_WON) {
            Bet bet = table.getBet();
            bank.putBet(bet);
        }
        if (states[0] == GameState.PUSH) {
            Bet bet = table.getBet();
            bank.putBet(bet);
        }

        // Split reward calculation
        if (states[1] == GameState.PLAYER_WON) {
            Bet bet = table.getBet();
            bank.putBet(bet);
        }
        if (states[1] == GameState.BLACKJACK_PLAYER_WON) {
            Bet bet = table.getBet();
            bank.putBet(bet);
        }
        if (states[1] == GameState.PUSH) {
            Bet bet = table.getBet();
            bank.putBet(bet);
        }

        List<Reward> rewards = table.getRewards();
        if (!rewards.isEmpty()) {
            bank.putRewards(rewards);
        }

        listener.onRoundOver(table);
    }

    /**
     * Automatic dealer turn
     * Dealer will try to grab cards until it win or lose
     */
    private void dealerTurn() {
        // Dealer's turn
        do {
            table.dealCardToDealer();
        } while (table.getStates()[0]== GameState.IN_PROGRESS);
    }

    /**
     * If the player wishes to take another card they signal to the dealer.
     * A single card is then played face up onto their hand. If the hand
     * total is less than 21 the player can choose to Hit again or Stand.
     * If the total is 21 the hand automatically stands. If the total is
     * over 21 the hand is bust, the player’s bet is taken by the house
     * and the turn to act passes to the next player.
     */
    public void hit() {
        table.dealCardToPlayer();
        if (table.getStates()[0] == GameState.DEALER_WON) {
            dealerTurn();
        }
    }

    /**
     * If the player is happy with the total they’ve been dealt they can
     * stand, taking no further action and passing to the next player.
     * The player can take this action after any of the other player
     * actions as long as their hand total is not more than 21.
     */
    public void stand() {
        if (table.canPlayerStand()) {
            dealerTurn();
        }
    }

    /**
     * If the player considers they have a favourable hand, generally a
     * total of 9, 10 or 11, they can choose to 'Double Down'. To do this
     * they place a second wager equal to their first beside their first
     * wager. A player who doubles down receives exactly one more card
     * face up and is then forced to stand regardless of the total.
     */
    public void doubleDown() {
        if (table.canPlayerDouble()) {
            // Return bet and get new one bet which is twice bigger
            Bet bet = table.getBet();
            bank.putBet(bet);
            table.setBet(bank.getBet(bet.getBetSize() * 2));
            // Deal new card to player
            table.dealCardToPlayer();
            dealerTurn();
        }
    }

    /**
     * If the player’s first two cards are of matching rank they can choose
     * to place an additional bet equal to their original bet and split the
     * cards into two hands. Where the player chooses to do this the cards
     * are separated and an additional card is dealt to complete each hand.
     */
    public void split() {
        if (table.canPlayerSplit()) {
            table.split();
        }
    }

    /**
     * If the player split his hand he can switch between hands to fill them
     * hitting additional cards
     */
    public void swapSplit() {
        if (table.canPlayerSwap()) {
            table.swapPlayerSplit();
        }
    }
}
